//Question1 
let r=4
Circumference=2*Math.PI*r
c=Circumference.toFixed(2)
console.log(c)

//Question2
let animalString = "cameldogcatlizard";
let andString = " and ";
cat=animalString.substr(8,3)
dog=animalString.substr(5,3)
and=andString.substring(0,5)
console.log(cat+and+dog)

//Question3
let person={
  FirstName:"Kanye",
  LastName:"West",
  BirthDate:"8 June 1977",
  AnnualIncome:"150000000.00"
}
console.log(person.FirstName+' '+person.LastName+" was born on "+person.BirthDate+" and has an annual income of $"+person.AnnualIncome)

//Question4 
var number1, number2;
//RHS generates a random number between 1 and 10 inclusive
number1 = Math.floor((Math.random() * 10) + 1);
//RHS generates a random number between 1 and 10 inclusive
number2 = Math.floor((Math.random() * 10) + 1);
console.log("number1 = " + number1 + " number2 = " + number2);
let number3= number1
number1=number2
number2=number3
console.log("number1 = " + number1 + " number2 = " + number2);

//Question5 
let year;
let yearNot2015Or2016;
year = 2014;
yearNot2015Or2016 = year !== 2015 && year !== 2016;
console.log(yearNot2015Or2016);