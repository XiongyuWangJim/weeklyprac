//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output =[54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19,-51, -17, -25]

    let outPutArea = document.getElementById("outputArea1") 
    let posodd=[];
    let negeven=[];
    for(let i=0;i<output.length;i++){
        if(output[i]>0 && output[i]%2 !== 0){
         posodd.push(output[i])   
        }
        else if (output[i]<0 && output[i]%2 === 0){
         negeven.push(output[i])   
        }
        else{
            
        }
    }

    outPutArea.innerText = "Positive Odd:"+ posodd +"\n" +"Negative Even:"+negeven
    
}

function question2(){
    let one=0
    let two=0
    let three=0
    let four=0
    let five=0
    let six=0
    for (let i=0;i<60000;i++){
        let dice=Math.floor(Math.random()* 6 )+1
        if (dice==1){
            one+=1;
        }
        else if (dice==2){
            two+=1;
        }
        else if (dice==3){
            three+=1;
        }
        else if (dice==4){
            four+=1;
        }
        else if (dice==5){
            five+=1;
        }
        else if (dice==6){
            six+=1;
        }
    }
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = "Frequency of die rolls" +"\n"+"1: "+one+"\n"+"2: "+two+"\n"+"3: "+three+"\n"+"4: "+four+"\n"+"5: "+five+"\n"+"6: "+six
}


function question3(){
    let roll=[0,0,0,0,0,0,0];
let die;
for(i=1;i<=60000;i++){
  die=Math.floor(Math.random()* 6 )+1
  roll[die]++
}
let answer='Frequency of die rolls'+'\n'
for(j=1;j<roll.length;j++){
  answer+=j+':'+roll[j]+'\n'
}
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = answer
}



function question4(){
let roll=[0,0,0,0,0,0,0];
let die;
for(i=1;i<=60000;i++){
  die=Math.floor(Math.random()* 6 )+1
  roll[die]++
}
var dierolls={
  Frequencies:{
  1:roll[1],
  2:roll[2],
  3:roll[3],
  4:roll[4],
  5:roll[5],
  6:roll[6],
  },
  Total:60000,
  Exceptions:""
}

for(let j=1;j<=6;j++){
  if (Math.abs(dierolls.Frequencies[j]-10000)>100){
    dierolls.Exceptions +=j
    dierolls.Exceptions +=" "
}
}
let a='Frequency of dice rolls'+"\n"+'Total rolls: ' +dierolls.Total+'\n'
for (let prop in dierolls.Frequencies){
  a+= prop+':'+dierolls.Frequencies[prop]+'\n'
}
a+='Exceptions: '+dierolls.Exceptions
    

    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText =a
}


function question5(){
   let person = {
 name: "Jane",
 income: 127050
}
   let tax=0
   if(person.income<18200){
       tax=0
   }else if(person.income<37000){
       tax=((person.income- 18200)*0.19)
   }else if(person.income<90000){
       tax=(3572+(person.income- 37000)*0.325)
   }else if(person.income<180000){
       tax=(20797+(person.income- 90000)*0.37)
   }else {
       tax=(54097+(person.income- 180000)*0.45)
   }
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText =  person.name+"'s income is:"+person.income+", and her tax owned is:$"+tax
}