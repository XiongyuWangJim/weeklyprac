function doIt(){
    let num1Ref= document.getElementById("number1")
    let num2Ref= document.getElementById("number2")
    let num3Ref= document.getElementById("number3")
    let answerRef=document.getElementById("answer")
    let oddevenRef=document.getElementById("oddeven")
    answer = Number(num1Ref.value) + Number(num2Ref.value) + Number(num3Ref.value)
    answerRef.innerText= answer
     if(answer>=0){
         answerRef.className="positive"
     }
     else{
         answerRef.className="negative"
     }
     if(answer%2==0){
         oddevenRef.innerHTML="(even)"
         oddevenRef.className="even"
     }
     else{
         oddevenRef.innerHTML="(odd)"
         oddevenRef.className="odd"
     }
    
}
