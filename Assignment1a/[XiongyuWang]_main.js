"use strict"

let bookingData = [
 {
 time: "08:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "09:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "10:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "11:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "12:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "13:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "14:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "15:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "16:00",
 reason: "",
 label: "",
 booked: false
 },{
 time: "17:00",
 reason: "",
 label: "",
 booked: false
 }
];



/* Task 1 */
function BookRoom(time, reason, label) {
  let index = bookingData.findIndex((element) => {
    return element.time == time;
  });
  bookingData[index].reason = reason;
    //assign user's reason input to BookingData 
  bookingData[index].label = label;
     //assign user's text input to BookingData 
  bookingData[index].booked = true;
     //assign user booker or non-booked to BookingData 
}
//BookRoom function used findindex to find all about time data in bookingData as index
/* Task 2 */
function checkroombooking(time) {
  let index = bookingData.findIndex((element) => {
    return element.time == time;
      //find some time is equal select time and save in bookingData.
  });
  console.log(index)
  return bookingData[index].booked;
    
}
//checkroombooking according to index and check room is booked or not within ture or false
/* Task 3 */

function clearRoomBookings() {
  let result = Array.from(bookingData, (element) => {
    return {
      time: element.time,
      reason: "",
      label: "",
      booked: false
        // let each element be intial reason is "" label is "" and booked is false
    }
  })
  bookingData = result;
  console.log(bookingData)
}
//revert some data we have inputed and let result equal empty data.
/* task5 */
function updateDisplay() {
  var html = '';
  bookingData.forEach((element) => {
    html += `
    <p>${element.time}:`
    // check each time's statement
    element.booked == false ?
        //condition
      html += 'Available' :
    // if true +=available which mean not book
      html += `Not Available ( ${element.label} )`
    //if not true += Not available and update what user's input of text by conditional operator
      html += `</p>  `
      //return new line
  })
  document.querySelector('#output').innerHTML = html;
}
//update some information(reason and text) in to output which list of time and available
/* task6 */
function doBooking() {
  let UserSelect = document.getElementById("inputTime");
  let index = UserSelect.selectedIndex;
  let time = UserSelect.options[index].value;
  let inputReason = document.querySelector('#inputReason').value;
  let inputLabel = document.querySelector('#inputLabel').value;
  if (time == '') {
    alert('Please select time');
    return false;
      //if time is empty would alert
  } else if (inputReason == '') {
    alert('pleace input Booking Reason');
    return false;
      // if reason is empty would alert
  } else if (inputLabel == '') {
    alert('pleace input Booking Text')
    return false
    //if text is empty would alert
  } else if (checkroombooking(time)) {
    alert('This time is not predetermined')
    return false;
      //check time is available or not and alert
  } else {
      confirm('Are you sure book this time for room ')
      //check all have input
    BookRoom(time, inputReason, inputLabel)
    updateDisplay()
    //call undatedisplay function
  }
}
//i use if and else if to check  time input, reason input and label input Then try tp update in each index's reason,text and time
/* task7 */
function clearAllBookings() {
  let ConfirmWord= confirm('Make sure to clear all reservations for the day')
  //confirm of clear all booked room data with Clear all room button
  if (ConfirmWord) {
    clearRoomBookings()
    updateDisplay()
    //call clearroombooking function and update data in BookingRoom's time/reason/text
  };
//
}
/* task8 */
function updateDayTime() {
  var str = "";
  var now = new Date();
  str += now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
  document.getElementById("timeNow").innerHTML = str;
}
//clock function to show local time by time in timeNow.
/* task9 */
window.onload = function () {
  setInterval(updateDayTime, 1000);
  updateDisplay()
  //create a timer of each 1000ms
}