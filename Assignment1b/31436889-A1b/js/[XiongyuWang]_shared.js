"use strict";
// Keys for localStorage0422
const STUDENT_INDEX_KEY = "studentIndex";/* store the selected student’s index (position) in the queue */
const STUDENT_QUEUE_KEY = "queueIndex";/*store the selected student’s queue index in the list of queues  */
const APP_DATA_KEY = "consultationAppData";/*  store the Session instance for this app */

class Student {
    constructor() {
        this._fullName = "";
        this._studentId = "";
        this._problem = "";
    }
    get fullName() {
        return this._fullName;
    }
    get studentId() {
        return this._studentId;
    }
    get problem() {
        return this._problem;
    }
    set fullName(fullName) {
        this._fullName = fullName
    }
    set studentId(studentId) {
        this._studentId = studentId
    }
    set problem(problem) {
        this._problem = problem
    }
    fromData(data) {
        this._fullName = data._fullName;
        this._studentId = data._studentId;
        this._problem = data._problem;
    }
}
//Student class record each student's information which is this student's name, student id and problem
class Session {
    
    constructor() {
        this._startTime = new Date();
        this._queue = []
    }
    get startTime() {
        return this._startTime
    }
    set startTime(startTime) {
        this._startTime = startTime;
    }
    get queue() {
        return this._queue
    }
    set queue(queue) {
        this._queue = queue
    }
 
   
    addSubQueue() {
        let newdata = new Student();
        this._queue.push({ 'queue': [newdata] })
    }
    //addSubQueue is a function to let _queue add a new list and this list start from a newdata   
    addStudent(fullName, studentId, problem) {
        let student = new Student();
        let data = {
            _fullName: fullName,
            _studentId: studentId,
            _problem: problem,
        }
        student.fromData(data)
        let savelength = [];
        //empty savelength from begining
        console.log(this._queue)
        this._queue.forEach((ele) => {
            savelength.push(ele.queue.length)
        //count each queue's length in a new array 
        })
        console.log(savelength)
        let min = Math.min(...savelength);
        //get savelength(array)'s minimum 
        console.log(min)
        let minIndex = savelength.findIndex((ele) => {
            return ele == min
        })
        //find minimum queue length's index
        console.log(this._queue[minIndex])
        console.log(minIndex)
        this._queue[minIndex].queue.push(student)
        //let the minimum number's add to original array which is _queue
        console.log(this._queue)
    }
    //This is the way to let a new student add the shortest queue if two queue is equal then add to first queue.
    
    
    
    removeStudent(index, queueIndex) {

        this._queue[index].queue.splice(queueIndex, 1);
    }
    //This is the way to delete a student from a queue in _queue's position and in each queue's position.
    
    
    getStudent(index, queueIndex) {
        return this._queue[queueIndex].queue[index]
    }
    //getstudent is find in _queue's queue which is each student's position in queuelist then return it.
    fromData(data) {
        this._queue = data._queue; 
    }
}
 //Session class manage each student's queue in list can add student in shortest queue, can remove student in list and get stduent in list.

//Task 2
function checkLocalStorageDataExist(key) {
    let lockerData = localStorage.getItem(key);
    if (lockerData == null) {
        return false;
    } else {
        return true;
    }
}
//check localstorage's data is exist or not 

//Task 3
function updateLocalStorageData(key, data) {
    let jsonData = JSON.stringify(data)
    localStorage.setItem(key, jsonData);
}
//updata localstorage's data is exist or not 

//Task 4
function getLocalStorageData(key) {
    let jsonData = localStorage.getItem(key);
    let data = jsonData;
    try {
        data = JSON.parse(jsonData);
    }
    catch (e) {
        console.error(e);
    }
    finally {
        return data;
    }
}
// retrieve data from local storage using the key
//Task 5
let consultSession = new Session();

if (checkLocalStorageDataExist(APP_DATA_KEY)) {
    // if LS data does exist
    let data = getLocalStorageData(APP_DATA_KEY);
    console.log(data)
    consultSession.fromData(data)
    console.log(consultSession)
} else {
    // if LS data doesn’t exist
    consultSession.addSubQueue();
    consultSession.addSubQueue();
    updateLocalStorageData(APP_DATA_KEY, consultSession);

}
console.log(consultSession)
//Task 8

function view(STUDENT_INDEX_KEY, STUDENT_QUEUE_KEY) {
    window.location.href = `./[XiongyuWang]_view.html?STUDENT_INDEX_KEY=${STUDENT_INDEX_KEY}&STUDENT_QUEUE_KEY=${STUDENT_QUEUE_KEY}`
}
//view function take two parameters  (student position in queue) and queueIndex (index of queue in the array of queues) to use two key to store postion of queue's value and postion of list in queue's value then redirect the user to the [XiongyuWang]_view.html page. After that we can update on localstorage.
//Task 9

function markDone(index, queueIndex) {
    const r = confirm("Mark the student as seen");
    //confirm information
    if (r) {
        consultSession.removeStudent(queueIndex, index)
        //delete the student from queue in index page
        updateLocalStorageData(APP_DATA_KEY, consultSession);
        //update local storage information
        window.location.reload();
    }
}
