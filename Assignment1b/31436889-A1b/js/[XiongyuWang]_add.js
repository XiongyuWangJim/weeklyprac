"use strict";
//Task 6
function addStudent() {
    let fullName = document.querySelector('#fullName').value;
    let fullNamemsg = document.querySelector('#fullName_msg');
    let studentId = document.querySelector('#studentId').value;
    let studentIdmsg = document.querySelector('#studentId_msg');
    let problem = document.querySelector('#problem').value;
    let problemmsg = document.querySelector('#problem_msg');
    const regex = /^[1-3]{1}[0-9]{7}$/;
    if (fullName == '' || studentId == '' || problem == '') {
        if (fullName == '') {
            fullNamemsg.innerHTML = 'Please complete the information'
            // if user dont type name will have red Please complete the information from bottom of Name input
        }
        if (studentId == '') {
            studentIdmsg.innerHTML = 'Please complete the information'
            // if user dont type id will have red Please complete the information from bottom of id input
        }
        if (problem == '') {
            problemmsg.innerHTML = 'Please complete the information'
            // if user dont type problem will have red Please complete the information from bottom of Problem input
        }
        return false;
    } else if (!regex.test(studentId)) {
        studentIdmsg.innerHTML = 'The format is incorrect. Please re-enter it(The seven significant digits beginning with 1-3 plus 0-9)'
    //if student id is not start from 1-3 + seven 0-9 id will have red The format is incorrect. Please re-enter it(The seven significant digits beginning with 1-3 plus 0-9) from bottom of id input
    } else {
        
        consultSession.addStudent(fullName, studentId, problem)
        console.log(consultSession)
        updateLocalStorageData(APP_DATA_KEY, consultSession)
        //update information from local storage
        alert('add success')
        window.location.href = './[XiongyuWang]_index.html'
    }
}