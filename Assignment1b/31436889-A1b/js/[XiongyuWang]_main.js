"use strict";
//Task 7
writeCurrentDate()
function writeCurrentDate() {
   //show current time
    function getNow(s) {
        return s < 10 ? '0' + s : s;
        //condition is some number less than 10 will have '0'+ some number
    }
    var myDate = new Date();
    var h = myDate.getHours(); //get cunrrent hour
    var m = myDate.getMinutes(); //get current minute
    var s = myDate.getSeconds();//get current second

    var now = getNow(h) + ':' + getNow(m) + ":" + getNow(s);
    document.querySelector('#currentTime').innerHTML = now;
    var timer = setTimeout("writeCurrentDate()", 1000);
}
//Task 10

function displayList(data) {
    console.log(data)
    var html = '';
    data.forEach((ele, i) => {
        //this is a model of each student in index html page's looking have view button and delete button
        html += `
            <!-- begin queue 'list' -->
            <ul class="mdl-list">
                <h4>Queue ${i + 1}</h4>

                <!-- begin item in this queue -->`
        //using array function to change html's element for each student and using ${} to take vairable
        ele.queue.forEach((el, m) => {
            html += `
                                <li class="mdl-list__item mdl-list__item--three-line">
                                    <span class="mdl-list__item-primary-content">
                                        <i class="material-icons mdl-list__item-avatar">person</i>
                                        <span>${el._fullName}</span>
                                     </span>
                                    <span class="mdl-list__item-secondary-content">
                                        <a class="mdl-list__item-secondary-action" onclick="view(${m},${i})"><i
                                                class="material-icons">info</i></a>
                                    </span>
                                    <span class="mdl-list__item-secondary-content">
                                        <a class="mdl-list__item-secondary-action" onclick="markDone(${m},${i})"><i
                                                class="material-icons">done</i></a>
                                    </span>
                                </li>
                                <!-- end item in this queue -->
                                `
        })

        html += `<!-- end item in this queue -->
                        </ul>`
        
    });
    document.querySelector('#queueContent').innerHTML = html;

}

displayList(consultSession._queue)

 